CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------
Acme is the theme which students build during Chapter Three's "Level Up Your Themer" class. We usually make slight improvements to the theme after every class and will continue supporting this theme with those updates.

 * For a full description of the theme, visit the project page:
   https://drupal.org/project/acme
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/acme


REQUIREMENTS
------------

This theme requires the Zentropy base theme (https://drupal.org/project/zentropy).


INSTALLATION
------------
 * Install as usual, see https://drupal.org/getting-started/install-contrib/themes for further information.


MAINTAINERS
-----------
Current maintainers:
 * David Needham (https://drupal.org/user/191261)
 * Meghan Sweet (https://drupal.org/user/204490)

Other credits:
 * Jen Lampton (https://drupal.org/user/85586)
 * Designer: Floor van Herreweghe